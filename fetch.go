package main

import (
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"sort"
	"strconv"
)

type Torrent struct {
	title    string
	link     string
	seeders  int
	leechers int
}

type byLeechers []Torrent

func (v byLeechers) Len() int           { return len(v) }
func (v byLeechers) Swap(i, j int)      { v[i], v[j] = v[j], v[i] }
func (v byLeechers) Less(i, j int) bool { return v[j].leechers < v[i].leechers }

func (torrent Torrent) SayHi() {
	fmt.Printf("%s %d %d\n", torrent.title, torrent.seeders, torrent.leechers)
}

func fetch(address string, c chan []Torrent) {

	torrents := make([]Torrent, 0, 30)
	doc, err := goquery.NewDocument(address)

	if err == nil {
		doc.Find("table#searchResult tr").Slice(1, 31).Each(func(i int, row *goquery.Selection) {
			title := row.Find("td div.detName a").Text()
			link, _ := row.Find("td div.detName a").Attr("href")

			seeders_raw := row.Find("td").Eq(2).Text()
			seeders, _ := strconv.Atoi(seeders_raw)

			leechers_raw := row.Find("td").Eq(3).Text()
			leechers, _ := strconv.Atoi(leechers_raw)

			link = "http://thepiratebay.com" + link

			torrent := Torrent{title: title, link: link, seeders: seeders, leechers: leechers}
			torrents = append(torrents, torrent)
		})
	}

	c <- torrents

}

const (
	// movieAdress = "http://194.71.107.27/browse/201/%d/3"
	movieAdress = "http://thepiratebay.sx/browse/201/%d/3"
)

func main() {
	pages := 10

	c := make(chan []Torrent)

	for i := 0; i < pages; i++ {
		address := fmt.Sprintf(movieAdress, i)
		go fetch(address, c)
	}

	result := make([]Torrent, 0, pages*30)

	for i := 0; i < pages; i++ {
		fmt.Println(i)
		if torrents := <-c; len(torrents) > 0 {
			result = append(result, torrents...)
		}
	}

	sort.Sort(byLeechers(result))

	for i, torrent := range result[:30] {
		fmt.Println(i, torrent.title, torrent.seeders, torrent.leechers)
	}
}
